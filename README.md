# What is Guardians
Guardians makes the most important of your files safe, by watching any change on your computer, and sending the new Data to a server.

It handles file saving whenever they are, so you haven't to change your habits, not move every files you want to store. Just run Guardians & let it know the files it has to manage.

You  can configure what to do for every path, including folders & files, in order to only save what it's really important for you. 

Whenever you want to retrieve your files, just ask and Guardians will retrieve them.

# How Guardians works
More precisely, any *Client* (i.e. a computer that watch files) watch some *Paths* (i.e. folders & files to keep safe). Each time you change a *Path*, your *Client* sends your data to the server.
The Client choose the best moment to contact the server, and handles any problems, such as unreachable server, or Client/Server desynchronisation.

On startup, the *Client* finds files that have been changed when Client was down.
As soon as the *Client* is ready, it watch your path and inform the server on any change.

# A note about terminology
Guardians project has several key concepts that you should know to understand messages and documentation.

Here is an overview of what you may need to understand Guardians.

## User
A *User* is, like every other site, the way to log in. You have to create an account in order to manage  your *Clients*.
You should have only one *User* account, and use it to register every *Client* you have.

## Client
A *Client* corresponds to a bot that automatically manages files to save.
You can create as much as *Client* you need. For example, you can choose to have a *Client* for each machine you want to manage, or a *Client* per user registered on your computer O.S if you want.

You should first configure what *Paths* your *Client* have to manage, before starting it. Feel free to start & stop your client on your machine when you need: a Client is just a bot which watch your files when you ask for.

## Path
A *Path* means a File System path (i.e. folder or file path) that your Client have to manage.
For each path, you can choose an action the *Client* will do for each file add/update/remove.

More information on the [Guardians-Client repo](https://framagit.org/guardians/guardians-client) 

## Server
Your *Clients* just watch for changes on your files and sends these data to a server.
This is why you have to instantiate a remote *Server*. This way, your data will always be safe into your server.
When it's possible, your *Clients* will send your files' data to the server, and handles any problems, such as server unreachable.

You can ask for a Client to retrieve your old data from the server: this Client will download the asked path and add it to your File System, at it own place.

More info on the [Guardians-Server repo](https://framagit.org/guardians/guardians-server) 
